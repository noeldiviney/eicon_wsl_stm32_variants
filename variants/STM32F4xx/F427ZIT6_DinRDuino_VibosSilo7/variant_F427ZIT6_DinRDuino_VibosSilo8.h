/*
  Copyright (c) 2011 Arduino.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _VARIANT_ARDUINO_STM32_
#define _VARIANT_ARDUINO_STM32_

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/
#include "PeripheralPins.h"

#ifdef __cplusplus
extern "C"{
#endif // __cplusplus

/*----------------------------------------------------------------------------
 *        Pins
 *----------------------------------------------------------------------------*/
extern const PinName digitalPin[];


//--------------------------------------------------------------------------------------------------------------
//  DinRDuino 		J3					  CPU Piinout			Cabinet Wire Signal		
#define PE1  0		// 1U_IN_1_24v+		  142_PE1_IN_1_24V+	    SILO_1_SHUTTER_INPUT 
#define PE0  1		// 2U_IN_2_24v+			141_PE0_IN_2_24V+	SILO_1_LOCK/UNLOCK 
#define PB9  2		// 3U_IN_3_24v+			140_PB9_IN_3_24V+	SILO_2_SHUTTER_INPUT 
#define PB8  3		// 4U_IN_4_24v+			139_PB8_IN_4_24V+	SILO_2_LOCK/UNLOCK 
#define PB7  4		// 5U_IN_5_24v+			137_PB7_IN_5_24V+	SILO_3_SHUTTER_INPUT 
#define PB6  5		// 6U_IN_6_24v+			136_PB6_IN_6_24V+	SILO_3_LOCK/UNLOCK 
#define PB5  6		// 7U_IN_7_24v+			135_PB5_IN_7_24V+	SILO_4_SHUTTER_INPUT 
#define PB4  7		// 8U_IN_8_24v+			134_PB4_IN_8_24V+	SILO_4_LOCK/UNLOCK 
#define PG12 8		// 9U_IN_9_24v+			127_PG12_IN_9_24V+	SILO_5_SHUTTER_INPUT 
#define PG9  9		// 10U_IN_10_24v+		124_PG9_IN_10_24V+	SILO_5_LOCK/UNLOCK 
#define PD7  10		// 11U_IN_11_24v+		123_PD7_IN_11_24V+	SILO_6_SHUTTER_INPUT 
#define PD6  11		// 12U_IN_12_24v+		122_PD6_IN_12_24V+	SILO_6_LOCK/UNLOCK 
					// 13U_GND									GND 
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino 		J4 Pins					CPU Piinout			Cabinet Wire Signal		
//	Power IN -		!l_24VDC_MINUS			Not used			24V Din Rail Power Supply
//	Power IN -		2l_24VDC_MINUS			Not used			24V Din Rail Power Supply
//					3l_NC					Not used			NC
//	Power IN +		4l_24VDC_PLUS			Not used			24V Din Rail Power Supply
//	Power IN +		5l_24VDC_PLUS			Not used			24V Din Rail Power Supply
//	Power OUT		6l_14V+ OUT				Not used			RSVD For 24V OUT
//	Power GND		7l_GND					Not used			GND
//	Power GND		8l_GND					Not used			GND
//	Power IN -		9l_12VDC_MINUS			Not used			12V Din Rail Power Supply
//	Power IN -		!0l_12VDC_MINUS			Not used			12V Din Rail Power Supply
//	Power IN/OUT	11l_NC					Not used			NC
//	Power IN/OUT	12l_12VDC_PLUS			Not used			12V Din Rail Power Supply
//	Power IN/OUT	13l_12VDC_PLUS			Not used			12V Din Rail Power Supply
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino 		J5 Pins					CPU Piinout			Cabinet Wire Signal		
#define PD5  12		// 14U_IN_13_24v+		119_PD5_IN_13_24V+	SILO_7_SHUTTER_INPUT 
#define PD4  13		// 15U_IN_14_24v+		118_PD4_IN_14_24V+	SILO_7_LOCK/UNLOCK 
#define PD3  14		// 16U_IN_13_24v+		117_PD3_IN_15_24V+	SILO_8_SHUTTER_INPUT 
#define PD2  15		// 17U_IN_14_24v+		116_PD2_IN_16_24V+	SILO_8_LOCK/UNLOCK 
//					   18U GND									GND
#define PB1  16		// 19U_ADC_1			47_PB1_ADC12_9		TBD 
//					   20U SHIELD								SHIELD
#define PB0  17		// 21U_ADC_2			46_PB0_ADC12_8		TBD 
//					   22U SHIELD								SHIELD
#define PA6  18		// 23U_ADC_3			42_PA6_ADC12_6		TBD 
//					   24U SHIELD								SHIELD
#define PA3  19		// 25U_ADC_4			37_PA3_ADC123_3		TBD 
//					   26U SHIELD								SHIELD
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino 		J6 Pins					CPU Piinout			Cabinet Wire Signal		
#define PD8  20		// 14L_RS232_TX			77_PD8_RS232_TX		RS232_TX 5V TTL
//  				   15L RS485_MINUS		See RS485 Periph
//					   16L RS485_PLUS		See RS485 Periph 
#define PG7  21		// 17L_24V_SINK_1		92_PG7_24V_SINK_1	SILO_1_LED_ON
#define PG6  22		// 18L_24V_SINK_2		91_PG6_24V_SINK_2	SILO_2_LED_ON
#define PG5  23		// 19L_24V_SINK_3		90_PG5_24V_SINK_3	SILO_3_LED_ON
#define PG4  24		// 20L_24V_SINK_4		89_PG4_24V_SINK_4	SILO_4_LED_ON
#define PG3  25		// 21L_24V_SINK_5		88_PG3_24V_SINK_5	SILO_5_LED_ON
#define PD15 26		// 22L_24V_SINK_6		86_PD15_24V_SINK_6	SILO_6_LED_ON
#define PD14 27		// 23L_24V_SINK_7		85_PD14_24V_SINK_7	SILO_7_LED_ON
#define PD13 28		// 24L_24V_SINK_8		82_PD13_24V_SINK_8	SILO_8_LED_ON
#define PC11 29		// 25L_24V_SINK_9		112_PC11_24V_SINK_9	UNassigned DIO OUT
//		 			   26L_GND									GND
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino       J7 pins                 CPU Pinout          Cabinet Wire Signal
#define PF0  30     // See i2c SDA          10_PF0_I2_SDA       DAC3
#define PF1  31     // See I2C SCL          11_PF1_I2_SCL       DAC3
// 	I2C_2 SDA/SCL      27U_DAC3+                                DAC3 
//                     28U_SHIELD                               DAC3
//  I2C_2 SDA/SCL      29U_DAC4+                                DAC4 
//                     30U_SHIELD                               DAC4 
#define PA4  32     //                      40_PA4_DAC1
//  see DAC1 Periph    31U_DAC1+                                DAC1 + 
//                     32U_SHIELD                               DAC1 -
#define PA5  33     //                      41_PA5_DAC2 
//  see DAC2 Periph    33U_DAC2+                                DAC2 + 
//                     34U_SHIELD                               DAC2+ 
//                     35U_GND                                  GND 
//                     36U_GND                                  GND 
#define PD0  34     // 37U_CAN_MINUS        114_PD0_CAN1_RX     CANBUS MINUS		
#define PD1  35     // 38U_CAN_PLUS         115_PD1_CAN1_TX     CANBUS PLUS
//                     39U_GND									GND 
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino       J8 pins                 CPU Pinout           Cabinet Wire Signal
#define PC10 36     // 32L_24V_SINK_10      111_PC10_24V_SINK10  VIBRATION RUNNING LED
#define PA15 37     // 33L_24V_SINK_11      110_PA15_24V_SINK11  VIBRATOR 1 LED
//#define PA8  38     // 34L_24V_SINK_12      100_PA8_24V_SINK12   VIBRATOR 2 LED
//#define PC9  39     // 35L_24V_SINK_13      99_PC9_24V_SINK13    AUTO LED ON
#define PC8  40     // 36L_24V_SINK_14      98_PC8_24V_SINK14    UNASSIGNED DIO OUT
#define PD12 41     // 37L_24V_SINK_15      81_PD12_24V_SINK15   UNASSIGNED DIO OUT
#define PPD8 42     // 38L_24V_SINK_16      77_PD8_24V_SINK16    UNASSIGNED DIO OUT
//                     39L GND                                   GND
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino       J9 pins               CPU Pinout             Cabinet Wire Signal
#define PF10 43     //                    22_PF10_OUT_8_PICK   
#define PC0  44     //                    26_PC0_OUT_8_HOLD   
//                     40U                                       SOL 8 PICK/HOLD +
#define PF8  45     //                    20_PF8_OUT_7_PICK   
#define PF9  46     //                    21_PF9_OUT_7_HOLD   
//                     41U                                       SOL 7 PICK/HOLD +
#define PC2  47     // 42U                28_PC2_SWITCH_IN       SILO 4 SWITCH IN
#define PF6  48     //                    18_PF6_OUT_6_PICK   
#define PF7  49     //                    19_PF7_OUT_6_HOLD   
//                     43U                                       SOL 6 PICK/HOLD +
#define PF4  50     //                    14_PF4_OUT_5_PICK   
#define PF5  51     //                    15_PF5_OUT_5_HOLD   
//                     44U                                       SOL 5 PICK/HOLD +
#define PC3  52     // 45U                29_PC3_SWITCH_IN       SILO 3 SWITCH IN
#define PF2  53     //                    12_PF2_OUT_4_PICK   
#define PF3  54     //                    13_PF3_OUT_4_HOLD   
//                     46U                                       SOL 4 PICK/HOLD +
#define PE6  55     //                    5_PE6_OUT_3_PICK   
#define PC13 56     //                    7_PC13_OUT_3_HOLD   
//                     47U                                       SOL 3 PICK/HOLD +
#define PA0  57     // 48U                34_PA0_SWITCH_IN       SILO 2 SWITCH IN
#define PE4  58     //                    3_PE4_OUT_2_PICK   
#define PE5  59     //                    4_PE5_OUT_2_HOLD   
//                     49U                                       SOL 2 PICK/HOLD +
#define PB2  60     // 50U                48_PB2_SWITCH_IN       SILO 1 SWITCH IN
#define PE2  61     //                    1_PE2_OUT_1_PICK   
#define PE3  62     //                    2_PE3_OUT_1_HOLD   
//                     51U                                       SOL 1 PICK/HOLD +
//                     52U                                       GND
//--------------------------------------------------------------------------------------------------------------
//  DinRDuino       J10 pins              CPU Pinout              Cabinet Wire Signal
#define PE14 63     //                    67_PE14_OUT_16_PICK   
#define PE15 64     //                    68_PE15_OUT_16_HOLD   
//                     40L                                        SOL 16 PICK/HOLD +
#define PE12  65    //                    65_PE12_OUT_15_PICK   
#define PE13  66    //                    66_PE13_OUT_15_HOLD   
//                     41L                                        SOL 15 PICK/HOLD +
#define PG15 67     // 42L                132_PG15_START/STOP_SW  START/STOP SWITCH
#define PE10 68     //                    63_PE10_OUT_14_PICK   
#define PE11 69     //                    64_PE11_OUT_14_HOLD   
//                     43L                                        SOL 14 PICK/HOLD +
#define PE8  70     //                    59_PE8_OUT_13_PICK   
#define PE9  71     //                    60_PE9_OUT_13_HOLD   
//                     44L                                        SOL 13 PICK/HOLD +
#define PD11 72     // 45L                80_PD11_VIB_SEL_SW      VIBRATORS SELECT SWITCH
#define PG1  73     //                    57_PG1_OUT_12_PICK   
#define PE7  74     //                    58_PE7_OUT_12_HOLD   
//                     46L                                        SOL 12 PICK/HOLD +
#define PF15 75     //                    55_PF15_OUT_11_PICK   
#define PG0  76     //                    56_PG0_OUT_11_HOLD   
//                     47L                                        SOL 11 PICK/HOLD +
#define PD10 77     // 48L                79_PD10_SWITCH_IN       SILO 6 SWITCH IN
#define PF13 78     //                    53_PF13_OUT_10_PICK   
#define PF14 79     //                    54_PF14_OUT_10_HOLD   
//                     49L                                        SOL 10 PICK/HOLD +
#define PC12 80     // 50L                113_PC12_SWITCH_IN      SILO 5 SWITCH IN
#define PF11 81     //                    49_PF11_OUT_9_PICK   
#define PF12 82     //                    50_PF12_OUT_9_HOLD   
//                     51L                                        SOL 9 PICK/HOLD +
//                     52L                                        GND
//--------------------------------------------------------------------------------------------------------------
// Misc Pins
#define PC6  83
#define PB13 84
#define PB10 85
#define PB11 86

#define PA8	 87
#define PC9	 88
#define PA12 104

//--------------------------------------------------
// Added by Simon H . to get I2C working.
//--------------------------------------------------
//#define PB10 85
//#define PB11 86
//#define PIN_WIRE_SDA       (PB11)
//#define PIN_WIRE_SCL       (PB10)

//#define PF0  30
//#define PF1  31
#define PIN_WIRE_SDA       	 (PF0)
#define PIN_WIRE_SCL       	 (PF1)

//#define PB7		4
//#define PB6		5
#define PIN_WIRE1_SDA        (PB7)
#define PIN_WIRE1_SCL        (PB6)
//--------------------------------------------------


// This must be a literal
#define NUM_DIGITAL_PINS        96
// This must be a literal with a value less than or equal to to MAX_ANALOG_INPUTS
#define NUM_ANALOG_INPUTS       18
#define NUM_ANALOG_FIRST        78

// On-board LED pin number
#define LED_BUILTIN             PB0
#define LED_GREEN               LED_BUILTIN
#define LED_BLUE                PB7
#define LED_RED                 PB14

// On-board user button
#define USER_BTN                PC13

// Timer Definitions
// Do not use timer used by PWM pins when possible. See PinMap_PWM.
#define TIMER_TONE              TIM6

// Do not use basic timer: OC is required
#define TIMER_SERVO             TIM2  //TODO: advanced-control timers don't work

// UART Definitions
#define SERIAL_UART_INSTANCE    3 //Connected to ST-Link

// Serial pin used for console (ex: stlink)
// Rerquired by Firmata
//#define PIN_SERIAL_RX           77 // PD9
//#define PIN_SERIAL_TX           76 // PD8

#ifdef __cplusplus
} // extern "C"
#endif
/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus
// These serial port names are intended to allow libraries and architecture-neutral
// sketches to automatically default to the correct port name for a particular type
// of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
// the first hardware serial port whose RX/TX pins are not dedicated to another use.
//
// SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
//
// SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
//
// SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
//
// SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
//
// SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
//                            pins are NOT connected to anything by default.
#define SERIAL_PORT_MONITOR     Serial
#define SERIAL_PORT_HARDWARE    Serial
#define SERIAL_PORT_USBVIRTUAL	Serial
#endif

#endif /* _VARIANT_ARDUINO_STM32_ */
